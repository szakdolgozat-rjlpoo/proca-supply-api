package xyz.nergal.proca.supply.api;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class StockResponse {

    private Long amount;
}
