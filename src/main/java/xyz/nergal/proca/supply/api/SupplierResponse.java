package xyz.nergal.proca.supply.api;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class SupplierResponse {

    private String id;

    private String name;
}
